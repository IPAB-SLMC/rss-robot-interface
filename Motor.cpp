/*
 * Motor.cpp
 *
 *  Created on: 26 Sep 2014
 *      Author: Vladimir Ivan
 */

#include "Motor.h"
#include "RobotInterface.h"

Motor::Motor() : handle(0), id(0), version(0), inputs(0), motors(0)
{

}

Motor::~Motor()
{
	if(handle)
	{
		for(int i=0;i<motors;i++)
		{
			setVelocity(i,0.0);
		}
		CPhidget_close((CPhidgetHandle)handle);
		CPhidget_delete((CPhidgetHandle)handle);
		if(id) std::cout << type << " v" << version << " (" << id << ") closed\n";
	}
}

bool Motor::waitForAttachement(int time)
{
	int result;
	const char *err;
	if((result = CPhidget_waitForAttachment((CPhidgetHandle)handle, time)))
	{
		CPhidget_getErrorDescription(result, &err);
		std::cout << "Problem waiting for attachment: " << err << "\n";
		return false;
	}
	return true;
}

bool Motor::open(int id)
{
	CPhidgetMotorControl_create(&handle);
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)handle, Motor::motorAttachHandler, this);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)handle, Motor::motorDetachHandler, this);
	CPhidget_set_OnError_Handler((CPhidgetHandle)handle, RobotInterface::errorHandler, this);
	CPhidgetMotorControl_set_OnInputChange_Handler (handle, Motor::motorInputChangeHandler, this);
	CPhidgetMotorControl_set_OnCurrentChange_Handler (handle, Motor::motorCurrentChangeHandler, this);
	CPhidget_open((CPhidgetHandle)handle, id);
}

void Motor::setAcceleration(int id, double value)
{
	CPhidgetMotorControl_setAcceleration (handle, id, value);
}

void Motor::setVelocity(int id, double value)
{
	CPhidgetMotorControl_setVelocity (handle, id, value);
}

int Motor::motorAttachHandler(CPhidgetHandle MC, void *userptr)
{
	Motor* mot = (Motor*)userptr;
  const char *name;
  CPhidget_getDeviceName (MC, &name);
  int tmpID;
  CPhidget_getSerialNumber(MC, &tmpID);
  mot->id = tmpID;
  mot->name = name;

  CPhidget_getDeviceType(MC, &name);
  mot->type=name;
	CPhidget_getDeviceVersion(MC, &mot->version);

	CPhidgetMotorControl_getInputCount(mot->handle, &mot->inputs);
	CPhidgetMotorControl_getMotorCount(mot->handle, &mot->motors);

	mot->input.resize(mot->inputs);
	mot->velocity.resize(mot->motors);
	mot->current.resize(mot->motors);

  std::cout << mot->type << " v" << mot->version << " (" << mot->id << ") attached\n";
  return 0;
}

int Motor::motorDetachHandler(CPhidgetHandle MC, void *userptr)
{
  Motor* mot = (Motor*)userptr;
  std::cout << mot->type << " v" << mot->version << " (" << mot->id << ") detached\n";
  mot->id = 0;
  return 0;
}

int Motor::motorInputChangeHandler(CPhidgetMotorControlHandle MC, void *userptr, int Index, int State)
{
	Motor* mot = (Motor*)userptr;
	boost::mutex::scoped_lock lock(mot->input_mutex);
	mot->input[Index]=State;
  return 0;
}

int Motor::motorVelocityChangeHandler(CPhidgetMotorControlHandle MC, void *userptr, int Index, double Value)
{
	Motor* mot = (Motor*)userptr;
	boost::mutex::scoped_lock lock(mot->velocity_mutex);
	mot->velocity[Index]=Value;
	return 0;
}

int Motor::motorCurrentChangeHandler(CPhidgetMotorControlHandle MC, void *userptr, int Index, double Value)
{
	Motor* mot = (Motor*)userptr;
	boost::mutex::scoped_lock lock(mot->current_mutex);
	mot->current[Index]=Value;
	return 0;
}

int Motor::getID()
{
	return id;
}

int Motor::getVersion()
{
	return version;
}

int Motor::getNumInputs()
{
	return inputs;
}

int Motor::getNumMotors()
{
	return motors;
}

std::string Motor::getName()
{
	return name;
}

std::string Motor::getType()
{
	return type;
}

int Motor::getInput(int index)
{
	boost::mutex::scoped_lock lock(input_mutex);
	if(index<0 || index>=inputs) {std::cerr << "Input index out of bounds!"; return -1;}
	return input[index];
}

double Motor::getVelocity(int index)
{
	boost::mutex::scoped_lock lock(velocity_mutex);
	if(index<0 || index>=motors) {std::cerr << "Motor index out of bounds!"; return std::numeric_limits<double>::quiet_NaN();}
	return velocity[index];
}

double Motor::getCurrent(int index)
{
	boost::mutex::scoped_lock lock(current_mutex);
	if(index<0 || index>=motors) {std::cerr << "Motor index out of bounds!"; return std::numeric_limits<double>::quiet_NaN();}
	return current[index];
}

