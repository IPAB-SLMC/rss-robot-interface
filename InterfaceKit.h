/*
 * InterfaceKit.h
 *
 *  Created on: 26 Sep 2014
 *      Author: s0972326
 */

#ifndef INTERFACEKIT_H_
#define INTERFACEKIT_H_

#include <phidget21.h>
#include <string>
#include <iostream>
#include <vector>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

class InterfaceKit;

typedef boost::shared_ptr<InterfaceKit> InterfaceKit_ptr;

/**
 * Interface kit data structure
 */
class InterfaceKit
{
	public:

		/**
		 * \brief Constructor
		 */
		InterfaceKit();

		/**
		 * \brief Destructor
		 */
		virtual 	~InterfaceKit();

		/**
		 * \brief Opens communication with the board
		 * @param id Serial number of the device (-1 for any device)
		 */
		bool open(int id=-1);

		/**
		 * \brief Boad attach handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 */
		static int attachHandler(CPhidgetHandle MC, void *userptr);

		/**
		 * \brief Boad detach handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 */
		static int detachHandler(CPhidgetHandle MC, void *userptr);

		/**
		 * \brief Interface kit digital input change handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param Index Input id
		 * @param State Input state
		 */
		static int inputChangeHandler(CPhidgetInterfaceKitHandle MC, void *usrptr, int Index, int State);

		/**
		 * \brief Interface kit digital output change handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param Index Output id
		 * @param State Output state
		 */
		static int outputChangeHandler(CPhidgetInterfaceKitHandle MC, void *usrptr, int Index, int State);

		/**
		 * \brief Interface kit sensor change handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param Index Sensor id
		 * @param State Sensor state
		 */
		static int sensorChangeHandler(CPhidgetInterfaceKitHandle MC, void *usrptr, int Index, int State);

		/**
		 * \brief Returns serial number.
		 * @return Serial numebr
		 */
		int getID();

		/**
		 * \brief Returns version number.
		 * @retun Version
		 */
		int getVersion();

		/**
		 * \brief Returns number of inputs.
		 * @return Number of inputs
		 */
		int getNumInputs();

		/**
		 * \brief Returns number of outputs.
		 * @return Number of outputs
		 */
		int getNumOutputs();

		/**
		 * \brief Returns number of sensors.
		 * @return Number of sensors
		 */
		int getNumSensors();

		/**
		 * \brief Returns boards name.
		 * @return Board name
		 */
		std::string getName();

		/**
		 * \brief Returns board type.
		 * @return Board type
		 */
		std::string getType();

		/**
		 * \brief Returns input state
		 * @param index Input number
		 * @return Input state
		 */
		int getInput(int index);

		/**
		 * \brief Returns output state
		 * @param index Output number
		 * @return Output state
		 */
		int getOutput(int index);

		/**
		 * \brief Returns sensor state
		 * @param index Sensor number
		 * @return Sensor state
		 */
		int getSensor(int index);

		/**
		 * \brief Returns sensor change trigger threshold.
		 * @return Trigger threshold
		 */
		int getSensorTrigger(int index);

		/**
		 * \brief Sets sensor change trigger threshold.
		 * @param index Sensor id
		 * @param value Trigger thereshold
		 */
		void setSensorTrigger(int index, int value);

		/**
		 * \brief Returns sensor data rate.
		 * @return Data rate (ms)
		 */
		int getSensorDataRate(int index);

		/**
		 * \brief Returns min sensor data rate.
		 * @return Min data rate (ms)
		 */
		int getSensorDataRateMin(int index);

		/**
		 * \brief Returns max sensor data rate.
		 * @return Max data rate (ms)
		 */
		int getSensorDataRateMax(int index);

		/**
		 * \brief Sets sensor data rate.
		 * @param index Sensor id
		 * @param value Data rate (ms)
		 */
		void setSensorDataRate(int index, int value);

		/**
		 * \brief Sets output pin state
		 * @param index Output index
		 * @param value Output state
		 */
		void setOutput(int index, int value);

		/**
		 * \brief Returns 1 if the sensors readings are ratiometric.
		 * @retun 1 if ratiometric, 0 otherwise.
		 */
		int getRatiometric();

		/**
		 * \brief Sets the sensors to be ratiometric.
		 * @param Set this to one to make sensors ratiometric.
		 */
		void setRatiometric(int value);

		/**
		 * \brief Waits specified amount of time for the board to attach.
		 * @return False on time out.
		 */
		bool waitForAttachement(int time=10000);

  private:
		CPhidgetInterfaceKitHandle handle; //!< DC motor interface board handle
		int id; //!< Serial number
		int version; //!< Board version
		int inputs;  //!< Number of digital inputs
		int outputs;  //!< Number of digital outputs
		int sensors;  //!< Number of sensors
		std::string name;  //!< Name of the device
		std::string type;  //!< Device type
		std::vector<int> input;  //!< Input values
		std::vector<int> output;  //!< Output values
		std::vector<double> sensor;  //!< Sensor values
		boost::mutex input_mutex;
		boost::mutex output_mutex;
		boost::mutex sensor_mutex;
};

#endif /* INTERFACEKIT_H_ */
