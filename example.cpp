#include "RobotInterface.h"

// DC motor control demo
void DemoMotor()
{
	// Get robot instance (singleton)
	RobotInterface_ptr rob = RobotInterface::getInstance();
	// Time variable for smooth motion
	double t=0.0;
	// If the robot is running, otherwise stop this thread
	while(rob->isRunning())
	{
		// For each motor board
		for(int ii=0; ii< rob->Motors.size();ii++)
		{
			Motor_ptr m = rob->Motors[ii];
			// Only if its serial number is valid
			if(m->getID())
			{
				// Set acceleration
				m->setAcceleration(0,50.0);
				// Set velocity (sinusoidal), changes over time
				m->setVelocity(0,sin(t*0.05)*100.0);
			}
		}
		// Sleep for 0.01s
		wait(0.01);
		// Update the time variable
		t+=0.01;
	}
}

// Servo control demo
void DemoServo()
{
	// Get robot instance (singleton)
	RobotInterface_ptr rob = RobotInterface::getInstance();
	// Time variable for smooth motion
	double t=0.0;
	// If the robot is running, otherwise stop this thread
	while(rob->isRunning())
	{
		// For each servo board
		for( int ii=0; ii< rob->Servos.size();ii++)
		{
			Servo_ptr s = rob->Servos[ii];
			// Only if its serial number is valid
			if(s->getID())
			{
				// Get min and max servo positions
				double servoMin, servoMax;
				servoMin=s->getPositionMin(0);
				servoMax=s->getPositionMax(0);
				// Engage the servo (this is only required once)
				s->engage(0,true);
				// Set the servo position (sinusoid between min and max position)
				s->setPosition(0,(sin(t*0.05)+1.0)*(servoMax-servoMin)*0.5+servoMin);
			}
		}
		// Sleep for 0.01s
		wait(0.01);
		// Update the time variable
		t+=0.01;
	}
}

// Servo control demo for older servo boards
void DemoSimpleServo()
{
	// Get robot instance (singleton)
	RobotInterface_ptr rob = RobotInterface::getInstance();
	// Time variable for smooth motion
	double t=0.0;
	// If the robot is running, otherwise stop this thread
	while(rob->isRunning())
	{
		// For each servo board
		for( int ii=0; ii< rob->SimpleServos.size();ii++)
		{
			SimpleServo_ptr s = rob->SimpleServos[ii];
			// Only if its serial number is valid
			if(s->getID())
			{
				// Get min and max servo positions
				double servoMin, servoMax;
				servoMin=s->getPositionMin(0);
				servoMax=s->getPositionMax(0);
				// Engage the servo (this is only required once)
				s->engage(0,true);
				// Set the servo position (sinusoid between min and max position)
				s->setPosition(0,(sin(t*0.05)+1.0)*(servoMax-servoMin)*0.5+servoMin);
			}
		}
		// Sleep for 0.01s
		wait(0.01);
		// Update the time variable
		t+=0.01;
	}
}

// Interface kit demo
void DemoIFKit()
{
	// Get robot instance (singleton)
	RobotInterface_ptr rob = RobotInterface::getInstance();
	// If the robot is running, otherwise stop this thread
	while(rob->isRunning())
	{
		// For each interface kit board
		for( int ii=0; ii< rob->InterfaceKits.size();ii++)
		{
			InterfaceKit_ptr s = rob->InterfaceKits[ii];
			// Only if its serial number is valid
			if(s->getID())
			{
				// Print out the ID
				std::cout << "Interface kit (" << s->getID() << ")\n";
				// Print out all the sensor values
				for(int i=0;i<s->getNumSensors();i++)
				{
					std::cout << "Sensor " <<i<< ": " << s->getSensor(i)<<"\n";
				}
				// Print out all the digital input values
				for(int i=0;i<s->getNumInputs();i++)
				{
					std::cout << "Input " <<i<< ": " << s->getInput(i)<<"\n";
				}
			}
		}
		// Sleep for 1s
		wait(1.0);
	}
}

// Image capture demo
void DemoCamera()
{
	// Get robot instance (singleton)
	RobotInterface_ptr rob = RobotInterface::getInstance();

	// Create capture device
	VideoCapture cap(0);


	if (!cap.isOpened())  // if not successful, exit demo
	{
		std::cerr << "Cannot open the video camera.\n";
		return;
	}

	// Get the width of frames of the video
	double width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
	// Get the height of frames of the video
	double height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);

	// Captured image
	Mat frame;

	// Create a window called "MyVideo" for display (optional)
	namedWindow("MyVideo",CV_WINDOW_AUTOSIZE);

	// If the robot is running, otherwise stop this thread
	while(rob->isRunning())
	{
		// Capture image from camera
		bool ret = cap.read(frame);

		// Check for capture errors
		if (!ret) //if not success, break loop
		{
			std::cerr << "Cannot read a frame from video stream.\n";
			break;
		}

		// Show the frame in "MyVideo" window (optional)
		imshow("MyVideo", frame);

		// Wait for key input
		// When displaying the image, this call refreshes the window.
		// The regular wait() method won't update the display window!
		waitKey(30);
	}

	// Release the capture device
	cap.release();
	// Destroy all windows
  //destroyAllWindows();
}

int main(int argc, char* argv[])
{
	// Get robot instance (singleton)
	RobotInterface_ptr rob = RobotInterface::getInstance();
	// Start communication with the boards
  rob->start();

  // Run each demo program on a separate thread
  boost::thread threadServo(DemoServo);
  boost::thread threadSimpleServo(DemoSimpleServo);
  boost::thread threadMotor(DemoMotor);
  boost::thread threadIFK(DemoIFKit);
  boost::thread threadcamera(DemoCamera);

  // Wait until power button was pressed
  while(!rob->getPowerButtonPressed())
  {
  	wait(0.1);
  }
  // Stop the communication with the boards
  // All the demo programs check if the robot is communication is still alive.
  rob->stop();

  // Join all the demo program threads
  threadServo.join();
  threadSimpleServo.join();
  threadMotor.join();
  threadIFK.join();
  threadcamera.join();

  return 0;
}
