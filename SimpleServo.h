/*
 * SimpleSimpleServo.h
 *
 *  Created on: 26 Sep 2014
 *      Author: Vladimir Ivan
 */

#ifndef SIMPLESERVO_H_
#define SIMPLESERVO_H_

#include <phidget21.h>
#include <string>
#include <iostream>
#include <vector>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

class SimpleServo;

typedef boost::shared_ptr<SimpleServo> SimpleServo_ptr;

/**
 * \brief Simple servo motor data structure
 */
class SimpleServo
{
  public:

		/**
		 * \brief Constructor
		 */
		SimpleServo();

		/**
		 * \brief Destructor
		 */
		virtual ~SimpleServo();

		/**
		 * \brief Opens communication with the board
		 * @param id Serial number of the device (-1 for any device)
		 */
		bool open(int id=-1);

		/**
		 * \brief Simple servo motor attach handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 */
		static int motorAttachHandler(CPhidgetHandle MC, void *userptr);

		/**
		 * \brief Simple servo motor detach handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 */
		static int motorDetachHandler(CPhidgetHandle MC, void *userptr);

		/**
		 * \brief Simple servo motor position change handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param Index Input id
		 * @param State Input state
		 */
		static int motorPositionHandler(CPhidgetServoHandle MC, void *usrptr, int Index, double Value);

		/**
		 * \brief Sets simple servo position
		 * @param id Motor ID
		 * @param value Position
		 */
		void setPosition(int id, double value);

		/**
		 * \brief Gets simple servo min position
		 * @param id Motor ID
		 * @param value Position
		 */
		double getPositionMin(int id);

		/**
		 * \brief Gets simple servo max position
		 * @param id Motor ID
		 * @param value Position
		 */
		double getPositionMax(int id);

		/**
		 * \biref Engage simple servo motor
		 * @param id Motor ID
		 * @param value Set to true to engage the motor
		 */
		void engage(int id, bool value);

		/**
		 * \brief Returns serial number.
		 * @return Serial numebr
		 */
		int getID();

		/**
		 * \brief Returns version number.
		 * @retun Version
		 */
		int getVersion();

		/**
		 * \brief Returns number of motors.
		 * @return Number of motors
		 */
		int getNumMotors();

		/**
		 * \brief Returns boards name.
		 * @return Board name
		 */
		std::string getName();

		/**
		 * \brief Returns board type.
		 * @return Board type
		 */
		std::string getType();

		/**
		 * \brief Returns servo position (commanded)
		 * @param index Servo number
		 * @return Position
		 */
		double getPosition(int index);

		/**
		 * \brief Waits specified amount of time for the board to attach.
		 * @return False on time out.
		 */
		bool waitForAttachement(int time=10000);

private:
		CPhidgetServoHandle handle; //!< DC motor interface board handle
		int id; //!< Serial number
		int version; //!< Board version
		int motors;  //!<  Number of motors
		std::string name;  //!< Name of the device
		std::string type;  //!< Device type
		std::vector<double> position;  //!< Measured current
		boost::mutex position_mutex;

};

#endif /* SIMPLESERVO_H_ */
