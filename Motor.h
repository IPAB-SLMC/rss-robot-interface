/*
 * Motor.h
 *
 *  Created on: 26 Sep 2014
 *      Author: Vladimir Ivan
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include <phidget21.h>
#include <string>
#include <iostream>
#include <vector>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

class Motor;

typedef boost::shared_ptr<Motor> Motor_ptr;

/**
 * \brief DC motor data structure
 */
class Motor
{
  public:

		/**
		 * \brief Constructor
		 */
		Motor();

		/**
		 * \brief Destructor
		 */
		virtual ~Motor();

		/**
		 * \brief Opens communication with the board
		 * @param id Serial number of the device (-1 for any device)
		 */
		bool open(int id=-1);

		/**
		 * \brief DC motor attach handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 */
		static int motorAttachHandler(CPhidgetHandle MC, void *userptr);

		/**
		 * \brief DC motor detach handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 */
		static int motorDetachHandler(CPhidgetHandle MC, void *userptr);

		/**
		 * \brief DC motor digital input change handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param Index Input id
		 * @param State Input state
		 */
		static int motorInputChangeHandler(CPhidgetMotorControlHandle MC, void *usrptr, int Index, int State);

		/**
		 * \brief DC motor velocity change handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param Index Motor id
		 * @param Value Velocity
		 */
		static int motorVelocityChangeHandler(CPhidgetMotorControlHandle MC, void *usrptr, int Index, double Value);

		/**
		 * \brief DC motor current change handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param Index Motor id
		 * @param Value Motor current
		 */
		static int motorCurrentChangeHandler(CPhidgetMotorControlHandle MC, void *usrptr, int Index, double Value);

		/**
		 * \brief Sets motor acceleration
		 * @param id Motor ID
		 * @param value Acceleration
		 */
		void setAcceleration(int id, double value);

		/**
		 * \brief Sets motor velocity.
		 * @param id Motor ID
		 * @param value Velocity
		 */
		void setVelocity(int id, double value);

		/**
		 * \brief Returns serial number.
		 * @return Serial numebr
		 */
		int getID();

		/**
		 * \brief Returns version number.
		 * @retun Version
		 */
		int getVersion();

		/**
		 * \brief Returns number of inputs.
		 * @return Number of inputs
		 */
		int getNumInputs();

		/**
		 * \brief Returns number of motors.
		 * @return Number of motors
		 */
		int getNumMotors();

		/**
		 * \brief Returns boards name.
		 * @return Board name
		 */
		std::string getName();

		/**
		 * \brief Returns board type.
		 * @return Board type
		 */
		std::string getType();

		/**
		 * \brief Returns input state
		 * @param index Input number
		 * @return Input state
		 */
		int getInput(int index);

		/**
		 * \brief Returns motor velocity
		 * @param index Motor number
		 * @return Velocity
		 */
		double getVelocity(int index);

		/**
		 * \brief Returns motor current
		 * @param index Motor number
		 * @return Current
		 */
		double getCurrent(int index);

		/**
		 * \brief Waits specified amount of time for the board to attach.
		 * @return False on time out.
		 */
		bool waitForAttachement(int time=10000);

  private:
		CPhidgetMotorControlHandle handle; //!< DC motor interface board handle
		int id; //!< Serial number
		int version; //!< Board version
		int inputs;  //!< Number of digital inputs
		int motors;  //!<  Number of motors
		std::string name;  //!< Name of the device
		std::string type;  //!< Device type
		std::vector<int> input;  //!< Input values
		std::vector<double> velocity;  //!< Measured motor velocities
		std::vector<double> current;  //!< Measured current
		boost::mutex input_mutex;
		boost::mutex velocity_mutex;
		boost::mutex current_mutex;
};

#endif /* MOTOR_H_ */
