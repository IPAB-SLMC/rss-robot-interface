/*
 * Servo.cpp
 *
 *  Created on: 26 Sep 2014
 *      Author: Vladimir Ivan
 */

#include "Servo.h"
#include "RobotInterface.h"

Servo::Servo() : handle(0), id(0), version(0), motors(0)
{

}

Servo::~Servo()
{
	if(handle)
	{
		for(int i=0;i<motors;i++)
		{
			engage(i,false);
		}
		CPhidget_close((CPhidgetHandle)handle);
		CPhidget_delete((CPhidgetHandle)handle);
		if(id) std::cout << type << " v" << version << " (" << id << ") closed\n";
	}
}

bool Servo::waitForAttachement(int time)
{
	int result;
	const char *err;
	if((result = CPhidget_waitForAttachment((CPhidgetHandle)handle, time)))
	{
		CPhidget_getErrorDescription(result, &err);
		std::cout << "Problem waiting for attachment: " << err << "\n";
		return false;
	}
	return true;
}

bool Servo::open(int id)
{
	CPhidgetAdvancedServo_create(&handle);
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)handle, Servo::motorAttachHandler, this);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)handle, Servo::motorDetachHandler, this);
	CPhidget_set_OnError_Handler((CPhidgetHandle)handle, RobotInterface::errorHandler, this);
	CPhidgetAdvancedServo_set_OnPositionChange_Handler(handle, Servo::motorPositionHandler, this);
	CPhidget_open((CPhidgetHandle)handle, id);
}

int Servo::motorAttachHandler(CPhidgetHandle MC, void *userptr)
{
	Servo* mot = (Servo*)userptr;
	const char *name;
	CPhidget_getDeviceName (MC, &name);
	int tmpID;
	CPhidget_getSerialNumber(MC, &tmpID);
	mot->id = tmpID;
	mot->name = name;

	CPhidget_getDeviceType(MC, &name);
	mot->type=name;
	CPhidget_getDeviceVersion(MC, &mot->version);

	CPhidgetAdvancedServo_getMotorCount(mot->handle, &mot->motors);

	for(int i=0;i<mot->motors;i++)
	  CPhidgetAdvancedServo_setServoType(mot->handle,i,PHIDGET_SERVO_HITEC_HS322HD);

	mot->position.resize(mot->motors);

	std::cout << mot->type << " v" << mot->version << " (" << mot->id << ") attached\n";
	return 0;
}

int Servo::motorDetachHandler(CPhidgetHandle MC, void *userptr)
{
	Servo* mot = (Servo*)userptr;
	std::cout << mot->type << " v" << mot->version << " (" << mot->id << ") detached\n";
	mot->id = 0;
	return 0;
}

int Servo::motorPositionHandler(CPhidgetAdvancedServoHandle MC, void *userptr, int Index, double Value)
{
	Servo* mot = (Servo*)userptr;
	mot->position[Index]=Value;
	return 0;
}

void Servo::setAcceleration(int id, double value)
{
	CPhidgetAdvancedServo_setAcceleration(handle, id, value);
}

double Servo::getAccelerationMin(int id)
{
  double tmp;
  CPhidgetAdvancedServo_getAccelerationMin(handle, id, &tmp);
  return tmp;
}

double Servo::getAccelerationMax(int id)
{
	double tmp;
	CPhidgetAdvancedServo_getAccelerationMax(handle, id, &tmp);
	return tmp;
}

void Servo::setPosition(int id, double value)
{
	CPhidgetAdvancedServo_setPosition(handle, id, value);
}

double Servo::getPositionMin(int id)
{
	double tmp;
	CPhidgetAdvancedServo_getPositionMin(handle, id, &tmp);
	return tmp;
}

double Servo::getPositionMax(int id)
{
	double tmp;
	CPhidgetAdvancedServo_getPositionMax(handle, id, &tmp);
	return tmp;
}

void Servo::engage(int id, bool value)
{
	CPhidgetAdvancedServo_setEngaged(handle, id, value?1:0);
}

int Servo::getID()
{
	return id;
}

int Servo::getVersion()
{
	return version;
}

int Servo::getNumMotors()
{
	return motors;
}

std::string Servo::getName()
{
	return name;
}

std::string Servo::getType()
{
	return type;
}

double Servo::getPosition(int index)
{
	boost::mutex::scoped_lock lock(position_mutex);
	if(index<0 || index>=motors) {std::cerr << "Servo index out of bounds!"; return std::numeric_limits<double>::quiet_NaN();}
	return position[index];
}
