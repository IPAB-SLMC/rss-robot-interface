/*
 * SimpleSimpleServo.cpp
 *
 *  Created on: 26 Sep 2014
 *      Author: Vladimir Ivan
 */

#include "SimpleServo.h"
#include "RobotInterface.h"

SimpleServo::SimpleServo() : handle(0), id(0), version(0), motors(0)
{

}

SimpleServo::~SimpleServo()
{
	if(handle)
	{
		for(int i=0;i<motors;i++)
		{
			engage(i,false);
		}
		CPhidget_close((CPhidgetHandle)handle);
		CPhidget_delete((CPhidgetHandle)handle);
		if(id) std::cout << type << " v" << version << " (" << id << ") closed\n";
	}
}

bool SimpleServo::waitForAttachement(int time)
{
	int result;
	const char *err;
	if((result = CPhidget_waitForAttachment((CPhidgetHandle)handle, time)))
	{
		CPhidget_getErrorDescription(result, &err);
		std::cout << "Problem waiting for attachment: " << err << "\n";
		return false;
	}
	return true;
}

bool SimpleServo::open(int id)
{
	CPhidgetServo_create(&handle);
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)handle, SimpleServo::motorAttachHandler, this);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)handle, SimpleServo::motorDetachHandler, this);
	CPhidget_set_OnError_Handler((CPhidgetHandle)handle, RobotInterface::errorHandler, this);
	CPhidgetServo_set_OnPositionChange_Handler(handle, SimpleServo::motorPositionHandler, this);
	CPhidget_open((CPhidgetHandle)handle, id);
}

int SimpleServo::motorAttachHandler(CPhidgetHandle MC, void *userptr)
{
	SimpleServo* mot = (SimpleServo*)userptr;
	const char *name;
	CPhidget_getDeviceName (MC, &name);
	int tmpID;
	CPhidget_getSerialNumber(MC, &tmpID);
	mot->id = tmpID;
	mot->name = name;

	CPhidget_getDeviceType(MC, &name);
	mot->type=name;
	CPhidget_getDeviceVersion(MC, &mot->version);

	CPhidgetServo_getMotorCount(mot->handle, &mot->motors);

	for(int i=0;i<mot->motors;i++)
	  CPhidgetServo_setServoType(mot->handle,i,PHIDGET_SERVO_HITEC_HS322HD);

	mot->position.resize(mot->motors);

	std::cout << mot->type << " v" << mot->version << " (" << mot->id << ") attached\n";
	return 0;
}

int SimpleServo::motorDetachHandler(CPhidgetHandle MC, void *userptr)
{
	SimpleServo* mot = (SimpleServo*)userptr;
	std::cout << mot->type << " v" << mot->version << " (" << mot->id << ") detached\n";
	mot->id = 0;
	return 0;
}

int SimpleServo::motorPositionHandler(CPhidgetServoHandle MC, void *userptr, int Index, double Value)
{
	SimpleServo* mot = (SimpleServo*)userptr;
	mot->position[Index]=Value;
	return 0;
}

void SimpleServo::setPosition(int id, double value)
{
	CPhidgetServo_setPosition(handle, id, value);
}

double SimpleServo::getPositionMin(int id)
{
	double tmp;
	CPhidgetServo_getPositionMin(handle, id, &tmp);
	return tmp;
}

double SimpleServo::getPositionMax(int id)
{
	double tmp;
	CPhidgetServo_getPositionMax(handle, id, &tmp);
	return tmp;
}

void SimpleServo::engage(int id, bool value)
{
	CPhidgetServo_setEngaged(handle, id, value?1:0);
}

int SimpleServo::getID()
{
	return id;
}

int SimpleServo::getVersion()
{
	return version;
}

int SimpleServo::getNumMotors()
{
	return motors;
}

std::string SimpleServo::getName()
{
	return name;
}

std::string SimpleServo::getType()
{
	return type;
}

double SimpleServo::getPosition(int index)
{
	boost::mutex::scoped_lock lock(position_mutex);
	if(index<0 || index>=motors) {std::cerr << "Servo index out of bounds!"; return std::numeric_limits<double>::quiet_NaN();}
	return position[index];
}
