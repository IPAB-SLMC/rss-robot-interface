/*
 * Servo.h
 *
 *  Created on: 26 Sep 2014
 *      Author: Vladimir Ivan
 */

#ifndef SERVO_H_
#define SERVO_H_

#include <phidget21.h>
#include <string>
#include <iostream>
#include <vector>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

class Servo;

typedef boost::shared_ptr<Servo> Servo_ptr;

/**
 * \brief Servo motor data structure
 */
class Servo
{
  public:

		/**
		 * \brief Constructor
		 */
		Servo();

		/**
		 * \brief Destructor
		 */
		virtual ~Servo();

		/**
		 * \brief Opens communication with the board
		 * @param id Serial number of the device (-1 for any device)
		 */
		bool open(int id=-1);

		/**
		 * \brief Servo motor attach handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 */
		static int motorAttachHandler(CPhidgetHandle MC, void *userptr);

		/**
		 * \brief Servo motor detach handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 */
		static int motorDetachHandler(CPhidgetHandle MC, void *userptr);

		/**
		 * \brief Servo motor position change handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param Index Input id
		 * @param State Input state
		 */
		static int motorPositionHandler(CPhidgetAdvancedServoHandle MC, void *usrptr, int Index, double Value);

		/**
		 * \brief Sets motor acceleration
		 * @param id Motor ID
		 * @param value Acceleration
		 */
		void setAcceleration(int id, double value);

		/**
		 * \brief Gets min motor acceleration
		 * @param id Motor ID
		 * @param value Acceleration
		 */
		double getAccelerationMin(int id);

		/**
		 * \brief Gets max motor acceleration
		 * @param id Motor ID
		 * @param value Acceleration
		 */
		double getAccelerationMax(int id);

		/**
		 * \brief Sets servo position
		 * @param id Motor ID
		 * @param value Position
		 */
		void setPosition(int id, double value);

		/**
		 * \brief Gets servo min position
		 * @param id Motor ID
		 * @param value Position
		 */
		double getPositionMin(int id);

		/**
		 * \brief Gets servo max position
		 * @param id Motor ID
		 * @param value Position
		 */
		double getPositionMax(int id);

		/**
		 * \biref Engage servo motor
		 * @param id Motor ID
		 * @param value Set to true to engage the motor
		 */
		void engage(int id, bool value);

		/**
		 * \brief Returns serial number.
		 * @return Serial numebr
		 */
		int getID();

		/**
		 * \brief Returns version number.
		 * @retun Version
		 */
		int getVersion();

		/**
		 * \brief Returns number of motors.
		 * @return Number of motors
		 */
		int getNumMotors();

		/**
		 * \brief Returns boards name.
		 * @return Board name
		 */
		std::string getName();

		/**
		 * \brief Returns board type.
		 * @return Board type
		 */
		std::string getType();

		/**
		 * \brief Returns servo position (commanded)
		 * @param index Servo number
		 * @return Position
		 */
		double getPosition(int index);

		/**
		 * \brief Waits specified amount of time for the board to attach.
		 * @return False on time out.
		 */
		bool waitForAttachement(int time=10000);

  private:
		CPhidgetAdvancedServoHandle handle; //!< DC motor interface board handle
		int id; //!< Serial number
		int version; //!< Board version
		int motors;  //!<  Number of motors
		std::string name;  //!< Name of the device
		std::string type;  //!< Device type
		std::vector<double> position;  //!< Measured current
		boost::mutex position_mutex;

};

#endif /* SERVO_H_ */
