/*
 * RobotInterface.h
 *
 *  Created on: 26 Sep 2014
 *      Author: Vladimir Ivan
 */

#include <sys/time.h>
#include <libpowerbutton.h>
#include <libpowerbutton.h>
#include <opencv2/opencv.hpp>
#include <phidget21.h>
#include "Motor.h"
#include "Servo.h"
#include "SimpleServo.h"
#include "InterfaceKit.h"
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/thread.hpp>

int wait(double t);

class RobotInterface;

typedef boost::shared_ptr<RobotInterface> RobotInterface_ptr;

using namespace cv;

/**
 * \brief Robot interface class (singleton)
 */
class RobotInterface
{
  public:
	 /**
	  * \brief Get instance of this singleton class
	  */
   static RobotInterface_ptr getInstance();
   /**
    * \brief Destructor
    */
   virtual ~RobotInterface();

   /**
    * \biref Checks if power button was pressed since last check.
    * @return True if power button was pressed.
    */
   bool getPowerButtonPressed();

   /// DC motor board interfaces
   std::vector<Motor_ptr> Motors;
   /// Servo motor board interfaces
   std::vector<Servo_ptr> Servos;
   /// Simple servo motor board interfaces
   std::vector<SimpleServo_ptr> SimpleServos;
   /// Interface kit boards
   std::vector<InterfaceKit_ptr> InterfaceKits;

   /**
    * \brief Connects a motor board to the robot
    * @param id Serial number of the motor board (-1 for any)
    */
   void connectMotor(int id=-1);

   /**
		 * \brief Connects a servo board to the robot
		 * @param id Serial number of the servo board (-1 for any)
		 */
		void connectServo(int id=-1);

		/**
		 * \brief Connects a simle servo board to the robot
		 * @param id Serial number of the simple servo board (-1 for any)
		 */
		void connectSimpleServo(int id=-1);

		/**
		 * \brief Connects a interface kit board to the robot
		 * @param id Serial number of the interface kit board (-1 for any)
		 */
		void connectInterfaceKit(int id=-1);

   /**
		 * \brief Phidget error handler
		 * @param MC Handle
		 * @param userptr User data pointer
		 * @param ErrorCode Error code
		 * @param Description Error description
		 */
   static int errorHandler(CPhidgetHandle MC, void *userptr, int ErrorCode, const char *Description);

   /**
    * \brief Stops the robot and closes communication to all boards.
    */
   void stop();

   /**
    * \brief True if robot is running
    */
   bool isRunning();

   /**
    * \brief Connects one of each type of board and waits for them to attach.
    * @param timeout Time to wait for attachment of each board (ms).
    */
   void start(int timeout=1000);

  private:
    /**
     * Private contructor (singleton)
     */
    RobotInterface();

    /**
     * Private static instance (singleton)
     */
    static RobotInterface_ptr _instance;

    /// Thrue if the robot is running
    bool running;

};


