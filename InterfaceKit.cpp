/*
 * InterfaceKit.cpp
 *
 *  Created on: 26 Sep 2014
 *      Author: s0972326
 */

#include "InterfaceKit.h"
#include "RobotInterface.h"

InterfaceKit::InterfaceKit () : handle(0), id(0), version(0), inputs(0), outputs(0), sensors(0)
{

}

InterfaceKit::~InterfaceKit ()
{
	if(handle)
	{
		CPhidget_close((CPhidgetHandle)handle);
		CPhidget_delete((CPhidgetHandle)handle);
		if(id) std::cout << type << " v" << version << " (" << id << ") closed\n";
	}
}

bool InterfaceKit::waitForAttachement(int time)
{
	int result;
	const char *err;
	if((result = CPhidget_waitForAttachment((CPhidgetHandle)handle, time)))
	{
		CPhidget_getErrorDescription(result, &err);
		std::cout << "Problem waiting for attachment: " << err << "\n";
		return false;
	}
	return true;
}

bool InterfaceKit::open(int id)
{
	CPhidgetInterfaceKit_create(&handle);
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)handle, InterfaceKit::attachHandler, this);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)handle, InterfaceKit::detachHandler, this);
	CPhidget_set_OnError_Handler((CPhidgetHandle)handle, RobotInterface::errorHandler, this);
	CPhidgetInterfaceKit_set_OnInputChange_Handler(handle, InterfaceKit::inputChangeHandler, this);
	CPhidgetInterfaceKit_set_OnOutputChange_Handler (handle, InterfaceKit::outputChangeHandler, this);
	CPhidgetInterfaceKit_set_OnSensorChange_Handler(handle, InterfaceKit::sensorChangeHandler, this);
	CPhidget_open((CPhidgetHandle)handle, id);
}

int InterfaceKit::attachHandler(CPhidgetHandle MC, void *userptr)
{
	InterfaceKit* mot = (InterfaceKit*)userptr;
  const char *name;
  CPhidget_getDeviceName (MC, &name);
  int tmpID;
  CPhidget_getSerialNumber(MC, &tmpID);
  mot->id = tmpID;
  mot->name = name;

  CPhidget_getDeviceType(MC, &name);
  mot->type=name;
	CPhidget_getDeviceVersion(MC, &mot->version);

	CPhidgetInterfaceKit_getInputCount(mot->handle, &mot->inputs);
	CPhidgetInterfaceKit_getOutputCount(mot->handle, &mot->outputs);
	CPhidgetInterfaceKit_getSensorCount(mot->handle, &mot->sensors);

	mot->input.resize(mot->inputs);
	mot->output.resize(mot->inputs);
	mot->sensor.resize(mot->sensors);

  std::cout << mot->type << " v" << mot->version << " (" << mot->id << ") attached\n";
  return 0;
}

int InterfaceKit::detachHandler(CPhidgetHandle MC, void *userptr)
{
	InterfaceKit* mot = (InterfaceKit*)userptr;
  std::cout << mot->type << " v" << mot->version << " (" << mot->id << ") detached\n";
  mot->id = 0;
  return 0;
}

int InterfaceKit::inputChangeHandler(CPhidgetInterfaceKitHandle MC, void *userptr, int Index, int State)
{
	InterfaceKit* mot = (InterfaceKit*)userptr;
	boost::mutex::scoped_lock lock(mot->input_mutex);
	mot->input[Index]=State;
  return 0;
}

int InterfaceKit::outputChangeHandler(CPhidgetInterfaceKitHandle MC, void *userptr, int Index, int State)
{
	InterfaceKit* mot = (InterfaceKit*)userptr;
	boost::mutex::scoped_lock lock(mot->output_mutex);
	mot->output[Index]=State;
  return 0;
}

int InterfaceKit::sensorChangeHandler(CPhidgetInterfaceKitHandle MC, void *userptr, int Index, int State)
{
	InterfaceKit* mot = (InterfaceKit*)userptr;
	boost::mutex::scoped_lock lock(mot->sensor_mutex);
	mot->sensor[Index]=State;
  return 0;
}

int InterfaceKit::getID()
{
	return id;
}

int InterfaceKit::getVersion()
{
	return version;
}

int InterfaceKit::getNumInputs()
{
	return inputs;
}

int InterfaceKit::getNumOutputs()
{
	return outputs;
}

int InterfaceKit::getNumSensors()
{
	return sensors;
}

std::string InterfaceKit::getName()
{
	return name;
}

std::string InterfaceKit::getType()
{
	return type;
}

int InterfaceKit::getInput(int index)
{
	boost::mutex::scoped_lock lock(input_mutex);
	if(index<0 || index>=inputs) {std::cerr << "Input index out of bounds!"; return -1;}
	return input[index];
}

int InterfaceKit::getOutput(int index)
{
	boost::mutex::scoped_lock lock(output_mutex);
	if(index<0 || index>=outputs) {std::cerr << "Output index out of bounds!"; return -1;}
	return output[index];
}

int InterfaceKit::getSensor(int index)
{
	boost::mutex::scoped_lock lock(sensor_mutex);
	if(index<0 || index>=sensors) {std::cerr << "Sensor index out of bounds!"; return -1;}
	return sensor[index];
}

int InterfaceKit::getSensorTrigger(int index)
{
	if(index<0 || index>=sensors) {std::cerr << "Sensor index out of bounds!"; return -1;}
	int tmp;
	CPhidgetInterfaceKit_getSensorChangeTrigger(handle,index,&tmp);
	return tmp;
}

void InterfaceKit::setSensorTrigger(int index, int value)
{
	if(index<0 || index>=sensors) {std::cerr << "Sensor index out of bounds!"; return;}
	CPhidgetInterfaceKit_setSensorChangeTrigger(handle,index,value);
}

int InterfaceKit::getSensorDataRate(int index)
{
	if(index<0 || index>=sensors) {std::cerr << "Sensor index out of bounds!"; return -1;}
	int tmp;
	CPhidgetInterfaceKit_getDataRate(handle,index,&tmp);
	return tmp;
}

int InterfaceKit::getSensorDataRateMin(int index)
{
	if(index<0 || index>=sensors) {std::cerr << "Sensor index out of bounds!"; return -1;}
	int tmp;
	CPhidgetInterfaceKit_getDataRateMin(handle,index,&tmp);
	return tmp;
}

int InterfaceKit::getSensorDataRateMax(int index)
{
	if(index<0 || index>=sensors) {std::cerr << "Sensor index out of bounds!"; return -1;}
	int tmp;
	CPhidgetInterfaceKit_getDataRateMax(handle,index,&tmp);
	return tmp;
}

void InterfaceKit::setSensorDataRate(int index, int value)
{
	if(index<0 || index>=sensors) {std::cerr << "Sensor index out of bounds!"; return;}
	CPhidgetInterfaceKit_setDataRate(handle,index,value);
}

void InterfaceKit::setOutput(int index, int value)
{
	if(index<0 || index>=outputs) {std::cerr << "Output index out of bounds!"; return;}
	CPhidgetInterfaceKit_setOutputState(handle,index,value);
}

int InterfaceKit::getRatiometric()
{
	int tmp;
	CPhidgetInterfaceKit_getRatiometric(handle,&tmp);
	return tmp;
}

void InterfaceKit::setRatiometric(int value)
{
	CPhidgetInterfaceKit_setRatiometric(handle,value);
}
