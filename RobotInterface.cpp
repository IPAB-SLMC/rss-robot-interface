/*
 * RobotInterface.cpp
 *
 *  Created on: 26 Sep 2014
 *      Author: Vladimir Ivan
 */

#include "RobotInterface.h"

RobotInterface_ptr RobotInterface::_instance;

int wait(double t)
{
  struct timespec req={0};
  time_t sec=(int)t;
  req.tv_sec=sec;
  req.tv_nsec= (long)((t-(double)sec)*1e-9);
  while(nanosleep(&req,&req)==-1) continue;
  return 1;
}

RobotInterface_ptr RobotInterface::getInstance()
{
  if(RobotInterface::_instance == NULL) RobotInterface::_instance.reset(new RobotInterface());
  return RobotInterface::_instance;
}

RobotInterface::~RobotInterface()
{

}

RobotInterface::RobotInterface() : running(false)
{
  power_button_reset();
}

int RobotInterface::errorHandler(CPhidgetHandle MC, void *userptr, int ErrorCode, const char *Description)
{
  std::cerr << "Error " << ErrorCode << ": " << Description << "\n";
  return 0;
}

bool RobotInterface::getPowerButtonPressed()
{
  if(power_button_get_value()>0)
  {
    power_button_reset();
    return true;
  }
  return false;
}

void RobotInterface::connectMotor(int id)
{
	Motors.push_back(Motor_ptr(new Motor()));
	Motors.back()->open(id);
	running = true;
}

void RobotInterface::connectServo(int id)
{
	Servos.push_back(Servo_ptr(new Servo()));
	Servos.back()->open(id);
	running = true;
}

void RobotInterface::connectSimpleServo(int id)
{
	SimpleServos.push_back(SimpleServo_ptr(new SimpleServo()));
	SimpleServos.back()->open(id);
	running = true;
}

void RobotInterface::connectInterfaceKit(int id)
{
	InterfaceKits.push_back(InterfaceKit_ptr(new InterfaceKit()));
	InterfaceKits.back()->open(id);
	running = true;
}

void RobotInterface::stop()
{
	running = false;
	Motors.clear();
	Servos.clear();
	SimpleServos.clear();
	InterfaceKits.clear();
}

bool RobotInterface::isRunning()
{
	return running;
}

void RobotInterface::start(int timeout)
{
	connectMotor();
	connectServo();
	connectSimpleServo();
	connectInterfaceKit();
	bool ret = false;
	ret=ret || Motors[0]->waitForAttachement(timeout);
	ret=ret || Servos[0]->waitForAttachement(timeout);
	ret=ret || SimpleServos[0]->waitForAttachement(timeout);
	ret=ret || InterfaceKits[0]->waitForAttachement(timeout);
	running = running && ret;
}
