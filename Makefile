ARCH = $(shell uname -m)
ifeq ($(ARCH),x86_64)
CXXFLAGS = -fPIC
CFLAGS = -fPIC
endif

CXXFLAGS += -std=c++0x -I/usr/local/include/powerbutton/
LDFLAGS += `pkg-config opencv --cflags --libs` -lphidget21 -lpowerbutton -lboost_thread -lboost_system -L/usr/local/lib

BASE := $(shell cd ./;pwd -L)

SOURCE_FILES=RobotInterface.cpp Motor.cpp Servo.cpp SimpleServo.cpp InterfaceKit.cpp

OUTPUT=RobotInterface

CPATH	:= $(BASE)
LPATH	:= $(BASE):/usr/local/lib
LD_RUN_PATH += $(LPATH)
export CPATH
export LPATH
export LD_RUN_PATH

all:
	$(CXX) -shared $(SOURCE_FILES) -o lib$(OUTPUT).so $(CXXFLAGS) $(CFLAGS) $(LDFLAGS)
	$(CXX) example.cpp -o example -lRobotInterface $(CXXFLAGS) $(CFLAGS) $(LDFLAGS) 

example:
	$(CXX) example.cpp -o example -lRobotInterface $(CXXFLAGS) $(CFLAGS) $(LDFLAGS) 